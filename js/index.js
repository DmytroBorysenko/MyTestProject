var url = 'https://apiv2.bitcoinaverage.com/indices/global/ticker/';

var currency = 'USD'

var selection = document.querySelector('.head select');
var btcRadio = document.getElementById('btc-radio')
console.log(btcRadio)

selection.onchange = function (e) {
	currency = this.options[this.selectedIndex].value;
	renderETH();
	renderLTC();
	renderBTC();
};




function renderETH() {
	var pricesArray = ['hour', 'day', 'week', 'month'];
	var ethPrice = document.querySelector('.eth-price');
     $.getJSON(url + 'ETH' + currency, function(data) {
		pricesArray.forEach(function(el) {

			var element = document.querySelector('.Ethereum .' + el);
			element.innerHTML = data.changes.price[el];
			ethPrice.innerHTML = data.volume;

            
			element.classList.remove('red');
			if (data.changes.price[el] < 0) {
				element.classList.add('red');
			}

		});
	});

}

renderETH();

function renderLTC() {
	var pricesArray = ['hour', 'day', 'week', 'month'];
	var ltcPrice = document.querySelector('.ltc-price');
     $.getJSON(url + 'LTC' + currency, function(data) {
		pricesArray.forEach(function(el) {

			var element = document.querySelector('.Litecoin .' + el);
			
			element.innerHTML = data.changes.price[el];
			ltcPrice.innerHTML = data.volume;

			element.classList.remove('red');
			if (data.changes.price[el] < 0) {
				element.classList.add('red');
			}

		});
	});

}

renderLTC();

function renderBTC() {
	var pricesArray = ['hour', 'day', 'week', 'month'];
	var btcPrice = document.querySelector('.btc-price');
     $.getJSON(url + 'BTC' + currency, function(data) {
		pricesArray.forEach(function(el) {

			var element = document.querySelector('.Bitcoin .' + el);
			
			element.innerHTML = data.changes.price[el];
			btcPrice.innerHTML = data.volume;

			element.classList.remove('red');
			if (data.changes.price[el] < 0) {
				element.classList.add('red');
			}

		});
	});

}



renderBTC();


